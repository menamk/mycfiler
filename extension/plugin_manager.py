#!/usr/bin/env python27
# coding: utf-8
"""
from 2ch
http://codepad.org/u1KCYTIk
"""

# ./extension内に配置した *_plugin.* モジュールを読み込んでsetup関数を呼び出します。 
# setup内でwindow.command_XXX = command_XXX と登録して、config.py 側でキー・アサインといった利用を想定してます。 
# 登録するコマンド名は他と重複してしまわないように注意。 

# 自動読み込みを経由しないで使いたい場合は、自分で config.py configure() 内に import して setupを呼び出す処理を書きます。 
# import hello_plugin 
# hello_plugin.setup(window) 

# 自分の利用方法では、 
# 適当なフォルダに pythonスクリプトやよく使うプログラムのショートカット、batファイル等をまとめて置いて、(unixの/usr/local/bin的な場所 
# アクティブなpaneのパスや選択したファイルのリストを渡して起動って使い方が主です。内骨格本体はあまり拡張してません。 
# （いろいろとロードして内骨格本体のプロセスが肥大化するのは避けたいのと、内骨格への依存を避ける為。） 
# 内骨格内部でいろいろやるというよりは、外部アプリの起動拠点として使ってます。# ・・・なので、内骨格へフィードバックするようなことがあまりない

"""

# ./extension/hello_plugin.py

  def say_hello(window):
	print "Hello,", window

  def setup(window):
	print "setup hello_plugin"
	window.command_hello = lambda: say_hello(window)

# in config.py

  def configure(window):
	  import plugin_manager
	  plugin_manager.load_plugins(window, path="./extension")

"""

import os
from glob import glob

GLOB_PATTERN = "*_plugin.*"
DEFAULT_EXTENSION_PATH = "./extension"
DEFAULT_FORCE_RELOAD = True
ATTR_LOADED_PLUGINS = "_loaded_plugins"

def enum_plugins(path):
	# XXX: this may load .pyo .pyc which does not have .py
	uniq = lambda xs: list(set(xs))
	name = lambda x: os.path.splitext(os.path.basename(x))[0]
	files = glob(os.path.join(path, GLOB_PATTERN))
	return [x for x in uniq(map(name, files)) if not x.startswith("_")]



def load_plugins(parent,
				path=DEFAULT_EXTENSION_PATH,
				force_reload=DEFAULT_FORCE_RELOAD,
				_attr_loaded_plugins=ATTR_LOADED_PLUGINS):
	
	import traceback
	
	print("loading plugin ", path, "...");
	
	# Store loaded plugins for reload
	if force_reload:
		loaded_plugins = getattr(parent, _attr_loaded_plugins, {})
		setattr(parent, _attr_loaded_plugins, loaded_plugins)
	else:
		loaded_plugins = {}

	# Load/Reload plugins
	for name in enum_plugins(path):
		if name in loaded_plugins and force_reload:
			try:
				reload(loaded_plugins[name])
			except:
				traceback.print_exc()
			continue
			
		try:
			module = __import__(name)
			loaded_plugins[name] = module
		except:
			traceback.print_exc()
			continue

	# Call setup function
	for module in loaded_plugins.values():
		try:
			setup = getattr(module, "setup", None)
			if setup:
				setup(parent)
		except:
			traceback.print_exc()
			continue