# -*- coding: utf-8 -*-

"""
デフォルトの設定ファイルにあったコマンドを移動
"""
import os
import sys
import io
import urllib.parse
from cfiler import *

# --------------------------------------------------------------------
# F1 キーでヘルプファイルを表示する
def command_Help(window):
	print( "Helpを起動 :" )
	help_path = os.path.join( getAppExePath(), 'doc\\index.html' )
	shellExecute( None, help_path, "", "" )
	print( "Done.\n" )
#window.keymap[ "F1" ] = command_Help


# --------------------------------------------------------------------
# Shift-X キーでプログラム起動メニューを表示する
def command_ProgramMenu(window):

	def launch_InternetExplorer():
		shellExecute( None, r"C:\Program Files\Internet Explorer\iexplore.exe", "", "" )

	def launch_CommandPrompt():
		shellExecute( None, r"cmd.exe", "", window.activeFileList().getLocation() )

	items = [
		( "Internet Explorer", launch_InternetExplorer ),
		( "Command Prompt",    launch_CommandPrompt )
	]

	result = popMenu( window, "プログラム", items, 0 )
	if result<0 : return
	items[result][1]()

#window.keymap[ "S-X" ] = command_ProgramMenu


# --------------------------------------------------------------------
# "CheckDuplicate" コマンド
#   左右のペイン両方のアイテムを通して、内容が重複するファイルを検索します。
#   ファイルのサイズが一致するものについて、より詳細に比較を行います。
def command_CheckDuplicate(window):
	
	left_pane = window.leftPane()
	right_pane = window.rightPane()

	left_location = window.leftFileList().getLocation()
	right_location = window.rightFileList().getLocation()

	left_items = window.leftItems()
	right_items = window.rightItems()
	
	items = []
	for item in left_items:
		if not item.isdir() and hasattr(item,"getFullpath"):
			items.append( [ item, None, False ] )
	for item in right_items:
		if not item.isdir() and hasattr(item,"getFullpath"):
			items.append( [ item, None, False ] )
			
	if len(items)<=1:
		return

	result_left_items = set()
	result_right_items = set()
	message = [""]

	def jobCheckDuplicate( job_item ):

		def printBoth(s):
			print( s )
			message[0] += s + "\n"

		def appendResult(item):
			if item in left_items:
				result_left_items.add(item)
				printBoth( '   Left: %s' % item.getName() )
			else:
				result_right_items.add(item)
				printBoth( '  Right: %s' % item.getName() )

		def leftOrRight(item):
			if item in left_items:
				return 'Left'
			else:
				return 'Right'

		printBoth( '重複するファイルを検索 :' )

		# ビジーインジケータ On
		window.setProgressValue(None)
		
		# ファイルのMD5値を調べる
		import hashlib
		for i, item in enumerate(items):

			if job_item.isCanceled(): break
			if job_item.waitPaused():
				window.setProgressValue(None)

			digest = hashlib.md5(item[0].open().read(64*1024)).hexdigest()
			print( 'MD5 : %s : %s' % ( item[0].getName(), digest ) )
			items[i][1] = digest
		
		# ファイルサイズとハッシュでソート
		if not job_item.isCanceled():
			items.sort( key = lambda item: ( item[0].size(), item[1] ) )

		for i in range(len(items)):

			if job_item.isCanceled(): break
			if job_item.waitPaused():
				window.setProgressValue(None)
			
			item1 = items[i]
			if item1[2] : continue
			
			dumplicate_items = []
			dumplicate_filenames = [ item1[0].getFullpath() ]
			
			for k in range( i+1, len(items) ):

				if job_item.isCanceled(): break
				if job_item.waitPaused():
					window.setProgressValue(None)

				item2 = items[k]
				if item1[1] != item2[1] : break
				if item2[2] : continue
				if item2[0].getFullpath() in dumplicate_filenames :
					item2[2] = True
					continue
					
				print( '比較 : %5s : %s' % ( leftOrRight(item1[0]), item1[0].getName() ) )
				print( '     : %5s : %s …' % ( leftOrRight(item2[0]), item2[0].getName() ), )
				
				try:
					result = compareFile( item1[0].getFullpath(), item2[0].getFullpath(), shallow=1, schedule_handler=job_item.isCanceled )
				except CanceledError:
					print( '中断' )
					break

				if result:
					print( '一致' )
					dumplicate_items.append(item2)
					dumplicate_filenames.append(item2[0].getFullpath())
					item2[2] = True
				else:
					print( '不一致' )

				print( '' )

			if dumplicate_items:
				appendResult(item1[0])
				for item2 in dumplicate_items:
					appendResult(item2[0])
				printBoth("")
			
		message[0] += '\n'
		message[0] += '検索結果をファイルリストに反映しますか？(Enter/Esc):\n'

	def jobCheckDuplicateFinished( job_item ):
	
		# ビジーインジケータ Off
		window.clearProgress()

		if job_item.isCanceled():
			print( '中断しました.\n' )
		else:
			print( 'Done.\n' )

		if job_item.isCanceled(): return

		result = popResultWindow( window, "検索完了", message[0] )
		if not result: return

		window.leftJumpLister( lister_Custom( window, "[duplicate] ", left_location, list(result_left_items) ) )
		window.rightJumpLister( lister_Custom( window, "[duplicate] ", right_location, list(result_right_items) ) )

	job_item = ckit.JobItem( jobCheckDuplicate, jobCheckDuplicateFinished )
	window.taskEnqueue( job_item, "CheckDuplicate" )


# --------------------------------------------------------------------
# "CheckSimilar" コマンド 
#   左右のペイン両方のアイテムを通して、名前が似ているファイルを検索します。

def command_CheckSimilar(window):

	left_location = window.leftFileList().getLocation()
	right_location = window.rightFileList().getLocation()
	left_items = window.leftItems()
	right_items = window.rightItems()
	items = left_items + right_items

	result_left_items = set()
	result_right_items = set()
	message = [""]

	def jobCheckSimilar( job_item ):

		def printBoth(s):
			print( s )
			message[0] += s + "\n"
			
		def appendResult(item):
			if item in left_items:
				result_left_items.add(item)
				printBoth( '   Left: %s' % item.getName() )
			else:
				result_right_items.add(item)
				printBoth( '  Right: %s' % item.getName() )

		printBoth('名前が似ているファイルを検索 :')

		# ビジーインジケータ On
		window.setProgressValue(None)
		
		def to_charset(item):
			return ( item, set(item.getName().lower()) )
		item_charset_list = map( to_charset, items )
	
		for i in range(len(item_charset_list)-1):

			if job_item.isCanceled(): break
			if job_item.waitPaused():
				window.setProgressValue(None)

			item_charset1 = item_charset_list[i]
			for k in range( i+1, len(item_charset_list) ):

				if job_item.isCanceled(): break
				if job_item.waitPaused():
					window.setProgressValue(None)

				item_charset2 = item_charset_list[k]
				or_set = item_charset1[1].union(item_charset2[1])
				and_set = item_charset1[1].intersection(item_charset2[1])
				score = float(len(and_set)) / float(len(or_set))

				if score>=0.90:
					appendResult(item_charset1[0])
					appendResult(item_charset2[0])
					printBoth('')
					
		message[0] += '\n'
		message[0] += '検索結果をファイルリストに反映しますか？(Enter/Esc):\n'

	def jobCheckSimilarFinished( job_item ):
	
		# ビジーインジケータ Off
		window.clearProgress()

		if job_item.isCanceled():
			print( '中断しました.\n' )
		else:
			print( 'Done.\n' )

		if job_item.isCanceled(): return

		result = popResultWindow( window, "検索完了", message[0] )
		if not result: return

		window.leftJumpLister( lister_Custom( window, "[similar] ", left_location, list(result_left_items) ) )
		window.rightJumpLister( lister_Custom( window, "[similar] ", right_location, list(result_right_items) ) )

	job_item = ckit.JobItem( jobCheckSimilar, jobCheckSimilarFinished )
	window.taskEnqueue( job_item, "CheckSimilar" )
	
	
# --------------------------------------------------------------------
# "CheckEmpty" コマンド 
#   ファイルが入っていない空のディレクトリを検索します。
#   ディレクトリが入っていても、ファイルが入っていない場合は空とみなします。

def command_CheckEmpty(window):
	
	pane = window.activePane()
	location = window.activeFileList().getLocation()
	items = window.activeItems()

	result_items = []
	message = [""]

	def jobCheckEmpty( job_item ):

		def printBoth(s):
			print( s )
			message[0] += s + "\n"

		def appendResult(item):
			result_items.append(item)
			printBoth( '   %s' % item.getName() )

		printBoth( '空のディレクトリを検索 :' )

		# ビジーインジケータ On
		window.setProgressValue(None)

		for item in items:
			
			if not item.isdir() : continue
			
			if job_item.isCanceled(): break
			if job_item.waitPaused():
				window.setProgressValue(None)
			
			empty = True
			
			for root, dirs, files in item.walk(False):

				if job_item.isCanceled(): break
				if job_item.waitPaused():
					window.setProgressValue(None)

				if not empty : break
				for file in files:
					empty = False
					break
			
			if empty:
				appendResult(item)

		message[0] += '\n'
		message[0] += '検索結果をファイルリストに反映しますか？(Enter/Esc):\n'
			
	def jobCheckEmptyFinished( job_item ):
	
		# ビジーインジケータ Off
		window.clearProgress()

		if job_item.isCanceled():
			print( '中断しました.\n' )
		else:
			print( 'Done.\n' )

		if job_item.isCanceled(): return

		result = popResultWindow( window, "検索完了", message[0] )
		if not result: return

		window.jumpLister( pane, lister_Custom( window, "[empty] ", location, result_items ) )

	job_item = ckit.JobItem( jobCheckEmpty, jobCheckEmptyFinished )
	window.taskEnqueue( job_item, "CheckEmpty" )



# --------------------------------------------------------------------
def setup(window):
	window.command_Help = command_Help
	window.command_ProgramMenu = command_ProgramMenu
	window.command_CheckDuplicate = command_CheckDuplicate
	window.command_CheckSimilar = command_CheckSimilar
	window.command_CheckEmpty = command_CheckEmpty
