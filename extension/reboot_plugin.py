# coding: utf-8

# ./extension/reboot_plugin.py
# 2ch
# http://codepad.org/sH7y093r

def reboot(window):
	import os
	import sys
	import ckit
	from cfiler_msgbox import popMessageBox, MSGBOX_TYPE_YESNO, MSGBOX_RESULT_YES
	caption,message = u'確認',u'再起動します'
	result = popMessageBox(window, MSGBOX_TYPE_YESNO, caption, message)
	if result == MSGBOX_RESULT_YES:
		def _job_reboot(job_item):
			cfiler_path = os.path.abspath(sys.argv[0])
			window.saveState()
			ckit.shellExecute(None,None,cfiler_path,u"",u"")
			window.quit()
		job_item = ckit.JobItem(_job_reboot)
		window.taskEnqueue(job_item)

def setup(window):
	window.command_Reboot = lambda: reboot(window)
