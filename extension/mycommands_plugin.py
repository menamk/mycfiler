# -*- coding: utf-8 -*-
"""
実用・単発系自作コマンド
"""
import os
import sys

from cfiler import *

def setup(window):
	
	# --------------------------------------------------------------------
	def command_ClearStatusMessage(info):
		"""
		ステータスメッセージを消す
		"""
		window.clearStatusMessage()
	window.command_ClearStatusMessage = command_ClearStatusMessage
	
	# --------------------------------------------------------------------
	def command_WriteStatusMessage(info):
		"""
		入力してステータスメッセージに表示
		"""
		msg = window.commandLine("message:")
		window.setStatusMessage(msg)
	window.command_WriteStatusMessage = command_WriteStatusMessage
	
	# --------------------------------------------------------------------
	def command_MakeNewFile(info):
		"""
		ファイル名を選択して新しいファイルをアクティブペインに作成
		デフォルトファイル名は YYYY-MM-DD-HHMMSS.txt
		要 touch コマンド
		"""
		from datetime import datetime
		dt = datetime.today().strftime("%Y-%m-%d-%H%M%S")
		default_fname = dt + ".txt"
		
		fname = window.commandLine("filename:",text=default_fname)
		if fname == None:
			print("Make new file cancel.")
		else:
			file_oncursor = window.activeCursorItem().getFullpath()
			active_dir = os.path.dirname(file_oncursor)
			dest = os.path.join(active_dir, fname)

			print("create file "+dest+"...")
			import shlex
			from subprocess import call
			cmd = shlex.split("touch \"" + dest + "\"")
			ret = call(cmd)
			if ret == 0:
				print("Make new file success.")
			else:
				print("Make new file failed. code = "+str(ret))
	window.command_MakeNewFile = command_MakeNewFile
