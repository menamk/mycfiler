﻿import os
import sys
import io
import urllib.parse
import xml.etree.ElementTree

from cfiler import *


# 設定処理
def configure(window):
	# --------------------------------------------------------------------
	# 自作コマンド
	def command_ShowGitReposDir(info):
		"""
		gitレポジトリ管理されているディレクトリを検索
		"""
		pass
	

	# --------------------------------------------------------------------
	# Enter キーを押したときの動作をカスタマイズするためのフック
	def hook_Enter():

		if 0:
			print( "hook_Enter" )
			pane = window.activePane()
			item = pane.file_list.getItem(pane.cursor)

			# hook から True を返すと、デフォルトの動作がスキップされます
			return True

	window.enter_hook = hook_Enter

	# --------------------------------------------------------------------
	# テキストエディタを設定する
	import platform
	system = platform.system()
	if 1: # プログラムのファイルパスを設定 (単純な使用方法)
		if system == 'Windows':
			window.editor = "C:/Program Files (x86)/Notepad++/notepad++.exe"
		elif system == 'Darwin':
			window.editor = "Z:/Applications/TextWrangler.app"

	if 0: # 呼び出し可能オブジェクトを設定 (高度な使用方法)
		def editor( item, region, location ):
			shellExecute( None, "C:/Program Files (x86)/Notepad++/notepad++.exe", '"%s"'% item.getFullpath(), location )
		window.editor = editor

	# --------------------------------------------------------------------
	# テキスト差分エディタを設定する
	#
	#	この例では外部テキストマージツールとして、WinMerge( http://winmerge.org/ )
	#	を使用しています。
	#	必要に応じてインストールしてください。

	if 0: # プログラムのファイルパスを設定 (単純な使用方法)
		window.diff_editor = "c:/ols/winmerge/WinMergeU.exe"

	if 0: # 呼び出し可能オブジェクトを設定 (高度な使用方法)
		def diffEditor( left_item, right_item, location ):
			shellExecute( None, "c:/ols/winmerge/WinMergeU.exe", '"%s" "%s"'% ( left_item.getFullpath(), right_item.getFullpath() ), location )
		window.diff_editor = diffEditor

	# --------------------------------------------------------------------
	# ソースファイルでEnterされたときの関連付け処理
	def association_Video(item):
		shellExecute( None, r"wmplayer.exe", '/prefetch:7 /Play "%s"' % item.getFullpath(), "" )

	window.association_list += [
		( "*.mpg *.mpeg *.avi *.wmv", association_Video ),
	]
		
	# --------------------------------------------------------------------
	# ファイルアイテムの表示形式
	# 昨日以前については日時、今日については時間、を表示するアイテムの表示形式
	#
	#	引数:
	#		window	 : メインウインドウ
	#		item	 : アイテムオブジェクト
	#		width	 : 表示領域の幅
	#		userdata : ファイルリストの描画中に一貫して使われるユーザデータオブジェクト
	#
	def itemformat_Name_Ext_Size_YYYYMMDDorHHMMSS( window, item, width, userdata ):
		if item.isdir():
			str_size = "<DIR>"
		else:
			str_size = "%6s" % getFileSizeString(item.size())
		if not hasattr(userdata,"now"):
			userdata.now = time.localtime()
		t = item.time()
		if t[0]==userdata.now[0] and t[1]==userdata.now[1] and t[2]==userdata.now[2]:
			str_time = "  %02d:%02d:%02d" % ( t[3], t[4], t[5] )
		else:
			str_time = "%04d/%02d/%02d" % ( t[0]%10000, t[1], t[2] )

		str_size_time = "%s %s" % ( str_size, str_time )
		width = max(40,width)
		filename_width = width-len(str_size_time)

		if item.isdir():
			body, ext = item.name, None
		else:
			body, ext = splitExt(item.name)

		if ext:
			body_width = min(width,filename_width-6)
			return ( adjustStringWidth(window,body,body_width,ALIGN_LEFT,ELLIPSIS_RIGHT)
				   + adjustStringWidth(window,ext,6,ALIGN_LEFT,ELLIPSIS_NONE)
				   + str_size_time )
		else:
			return ( adjustStringWidth(window,body,filename_width,ALIGN_LEFT,ELLIPSIS_RIGHT)
				   + str_size_time )

	# Z キーで表示されるファイル表示形式リスト
	window.itemformat_list = [
		( "1 : 全て表示 : filename	.ext  99.9K YY/MM/DD HH:MM:SS", itemformat_Name_Ext_Size_YYMMDD_HHMMSS ),
		( "2 : 秒を省略 : filename	.ext  99.9K YY/MM/DD HH:MM",	itemformat_Name_Ext_Size_YYMMDD_HHMM ),
		( "3 : 日 or 時 : filename  .ext	99.9K YYYY/MM/DD",		  itemformat_Name_Ext_Size_YYYYMMDDorHHMMSS ),
		( "0 : 名前のみ : filename.ext",							itemformat_NameExt ),
	]
	# 表示形式の初期設定
	window.itemformat = itemformat_Name_Ext_Size_YYMMDD_HHMMSS

	# --------------------------------------------------------------------
	# Subversionでバージョン管理しているファイルだけを表示するフィルタ
	class filter_Subversion:
		svn_exe_path = "c:/Program Files/TortoiseSVN/bin/svn.exe"
		def __init__( self, nonsvn_dir_policy ):
			self.nonsvn_dir_policy = nonsvn_dir_policy
			self.cache = {}
			self.last_used = time.time()
		def _getSvnFiles( self, dirname ):		
			if dirname.replace("\\","/").find("/.svn/") >= 0:
				return set()
			filename_set = set()			
			cmd = [ filter_Subversion.svn_exe_path, "stat", "-q", "-v", "--xml", "--depth", "immediates" ]
			svn_output = io.StringIO()
			subprocess = SubProcess( cmd, cwd=dirname, env=None, stdout_write=svn_output.write )
			subprocess()
			
			try:
				elm1 = xml.etree.ElementTree.fromstring(svn_output.getvalue())
			except xml.etree.ElementTree.ParseError as e:
				for line in svn_output.getvalue().splitlines():
					if line.startswith("svn:"):
						print( line )
				return filename_set
			elm2 = elm1.find("target")
			for elm3 in elm2.findall("entry"):
				elm4 = elm3.find("wc-status")
				if elm4.get("item")=="unversioned": continue
				filename_set.add( elm3.get("path") )
			return filename_set

		def __call__( self, item ):
			now = time.time()
			if now - self.last_used > 3.0:
				self.cache = {}
			self.last_used = now
			if not hasattr(item,"getFullpath"): return False
			fullpath = item.getFullpath()		
			dirname, filename = os.path.split(fullpath)
			
			try:
				filename_set = self.cache[dirname]
			except KeyError:
				filename_set = self._getSvnFiles(dirname)
				self.cache[dirname] = filename_set

			if item.isdir() and not filename_set:
				return self.nonsvn_dir_policy
			return filename in filename_set
	
		def __str__(self):
			return "[svn]"

	window.filter_list += [
		( "SUBVERSION",	   filter_Subversion( nonsvn_dir_policy=True ) ),
	]

	window.select_filter_list += [
		( "SUBVERSION",	   filter_Subversion( nonsvn_dir_policy=False ) ),
	]
	
	# --------------------------------------------------------------------
	# 外部コマンドのロード
	import plugin_manager
	plugin_manager.load_plugins(window, path="./extension")
	
	# --------------------------------------------------------------------
	# コマンドランチャにコマンドを登録する

	window.launcher.command_list += [
		( "Help",			   window.command_Help ),
		( "CheckEmpty",		   window.command_CheckEmpty ),
		( "CheckDuplicate",	   window.command_CheckDuplicate ),
		( "CheckSimilar",	   window.command_CheckSimilar ),
	]

	# --------------------------------------------------------------------
	# J キーで表示されるジャンプリスト
	import platform
	system = platform.system()
	addlist = [];
	if system == 'Windows':
		addlist = [
		( "htdocs", "C:/xampp/htdocs" ),
		( "HOME", "C:/Users/myamamoto/" ),
		( "Documents", "C:/Users/myamamoto/Documents" ),
		( "Desktop", "C:/Users/myamamoto/Desktop" ),
		( "Downloads", u"C:/Users/myamamoto/Downloads" ),
		( "GoogleDrive", u"C:/Users/myamamoto/Google ドライブ" ),
		( "ProgramFiles(x86)", "C:/Program Files (x86)" ),
		( "Start up", u"C:/Users/myamamoto/AppData/Roaming/Microsoft/Windows/Start Menu/Programs/Startup" )
		]
	elif system == 'Darwin':
		addlist = [];
	window.jump_list += addlist

	# --------------------------------------------------------------------
	# ; キーで表示されるフィルタリスト
	window.filter_list += [
		( "ALL",			   filter_Default( "*" ) ),
		( "SOURCE",			   filter_Default( "*.cpp *.c *.h *.cs *.py *.pyw *.fx" ) ),
		( "WEBDEV",			   filter_Default( "*.html *.css *.sass *.scss *.js *.php" ) ),
		( "IMG",			filter_Default( "*.gif *.jpg *.jpeg *.png *.ps" ) ),
		( "BOOKMARK",		   filter_Bookmark() ),
	]

	# --------------------------------------------------------------------
	# " / @キーで表示されるフィルタ選択リスト
	window.select_filter_list += [
		( "SOURCE",		   filter_Default( "*.cpp *.c *.h *.cs *.py *.pyw *.fx", dir_policy=None ) ),
		( "BOOKMARK",	   filter_Bookmark(dir_policy=None) ),
	]

	# --------------------------------------------------------------------
	# アーカイブファイルのファイル名パターンとアーカイバの関連付け
	window.archiver_list = [
		( "*.zip *.jar *.apk",	ZipArchiver ),
		( "*.7z",				SevenZipArchiver ),
		( "*.tgz *.tar.gz",		TgzArchiver ),
		( "*.tbz2 *.tar.bz2",	Bz2Archiver ),
		( "*.lzh",				LhaArchiver ),
		( "*.rar",				RarArchiver ),
	]

	# --------------------------------------------------------------------
	# キーマップ
	# --------------------------------------------------------------------
	# キーのカスタム （キー設定101として）
	# J, K	上下カーソル移動	<J:ジャンプ, K:ファイル削除（非ゴミ箱）/ディレクトリ作成>
	# C-J, K	上下カーソル移動 ブックマークor選択ファイルのみ	<C-J: 検索結果に移動>
	# \		  ジャンプ
	# S-\	  パス入力移動  <ルートへ移動>
	# C-S-\ ルートへ移動
	# C-S	インクリメンタルサーチ
	# C-S-F	  ファイル名検索結果に移動	  <ファイル名検索>
	# D			削除
	# ;		コマンド入力（どうもvimのくせがついているようだ...） <パターンフィルタ>
	# .		パターンフィルタ
	# S-.	パターン入力フィルタ
	window.keymap[ "K" ] = window.command_CursorUp
	window.keymap[ "J" ] = window.command_CursorDown
	window.keymap[ "C-K" ] = window.command_CursorUpSelectedOrBookmark
	window.keymap[ "C-J" ] = window.command_CursorDownSelectedOrBookmark
	window.keymap[ "BackSlash" ] = window.command_JumpList
	window.keymap[ "S-BackSlash" ] = window.command_JumpInput
	window.keymap[ "C-S-BackSlash" ] = window.command_GotoRootDir
	window.keymap[ "C-S" ] = window.command_IncrementalSearch
	window.keymap[ "C-S-F" ] = window.command_JumpFound
	window.keymap[ "D" ] = window.command_Delete
	window.keymap[ "Semicolon" ] = window.command_CommandLine
	window.keymap[ "Period" ] = window.command_SetFilterList
	window.keymap[ "S-Period" ] = window.command_SetFilter
	# その他ユーザーコマンドなどへのマップ
	window.keymap[ "F1" ] = window.command_Help
	window.keymap[ "S-X" ] = window.command_ProgramMenu

	
# テキストビューアの設定処理
def configure_TextViewer(window):
	# --------------------------------------------------------------------
	# キーマップ
	# --------------------------------------------------------------------
	# キーのカスタム （キー設定101あふ互換として） lessみたいにする
	# J, K	上下カーソル移動
	# C-J, K	上下ページ移動
	# Space 次のページ		<次を検索>
	# S-Space	前のページ	<前を検索>
	# /		検索
	# N		次を検索
	# S-N	前を検索
	# Q		ビューアを閉じる
	#
	window.keymap[ "J" ] = window.command_ScrollDown
	window.keymap[ "K" ] = window.command_ScrollUp
	window.keymap[ "Space" ] = window.command_PageDown
	window.keymap[ "S-Space" ] = window.command_PageUp
	window.keymap[ "C-D" ] = window.command_PageDown
	window.keymap[ "C-U" ] = window.command_PageUp
	window.keymap[ "Slash" ] = window.command_Search
	window.keymap[ "N" ] = window.command_SearchNext
	window.keymap[ "S-N" ] = window.command_SearchPrev
	window.keymap[ "Q" ] = window.command_Close



# テキスト差分ビューアの設定処理
def configure_DiffViewer(window):
	pass

# イメージビューアの設定処理
def configure_ImageViewer(window):
	pass

# リストウインドウの設定処理
def configure_ListWindow(window):
	pass

